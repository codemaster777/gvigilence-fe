'use strict';

/* Directives */


angular.module('ngdemo.directives', []).
directive('appVersion', ['version', function(version) {
        return function(scope, elm, attrs) {
            elm.text(version);
        };
    }])
    /* .directive('fitAvailableHeight', function() {
        return function(scope, elm, attrs) {
            $(document).ready(function() {
                var headerHeight = $('header').outerHeight();
                var footerHeight = $('.footer').outerHeight();
                var viewportHeight = $(window).height();
                console.log(headerHeight, footerHeight, viewportHeight)

                elm.css('minHeight', viewportHeight - headerHeight - footerHeight);
            });
        };
    })
 */
 .directive ('fitAvailableHeight', function () {
		return function(scope, elm, attrs) {
		     function adjustHeight() {
			  var headerHeight = $('header').outerHeight();
			  var footerHeight = $('.footer').outerHeight();
			  var viewportHeight = $(window).height();
			  console.log(headerHeight, footerHeight, viewportHeight)
			
			  elm.css('minHeight', viewportHeight - headerHeight - footerHeight);
			}
			
		    $(document).ready(adjustHeight);
			$(window).resize(adjustHeight);
		};
  })
 
 
 

.directive("fileread", [function() {
    return {
        scope: {
            fileread: "="
        },
        link: function(scope, element, attributes) {
            element.bind("change", function(changeEvent) {
                var reader = new FileReader();
                reader.onload = function(loadEvent) {
                    scope.$apply(function() {
                        console.log(loadEvent.target.result);
                        scope.fileread = loadEvent.target.result;
                    });
                }
                reader.readAsDataURL(changeEvent.target.files[0]);
            });
        }
    }
}])

.directive("timeline", ['timesheetServices', function (timesheetServices) {
	return {
		restrict: 'E',
		template: '<div></div>',
		replace: true,
		link: function ($rootScope, scope, elem, attrs) {

			var container = elem[0];
			var timeline = null;
			scope.currentItem = null;
			scope.currentInput = null;
			var result = null;
			
			scope.loadTimeline = function () {
				timesheetServices.getTimes().timesheetList(scope.currentDate, function(data){
					if(timeline) {
						timeline.destroy();
						console.log('destoyed timeline');
					}
					init(data.result);
				});
			}

			$rootScope.currentDate = new Date();
			scope.currentWeek = null;
			scope.currentMonth = null;

			scope.getDailyReport = function () {
				scope.loadTimeline(scope.currentDate);
			}


			scope.save = function () {
				var currentItem = {
					start : scope.currentItem.start,
					end :scope.currentItem.end,
					oldStartdate : scope.currentItem.oldStartdate,
					oldEnddate : scope.currentItem.oldEnddate,
					empSmartId : scope.currentItem.empSmartId
				}

				timesheetServices.update().timesheetData(currentItem, function(data){
					if(data.result == 'success') {

						for(var i=0; i<result.length; i++) {
							if (result[i].empSmartId == scope.currentItem.empSmartId) {
								for (var j=0; j<result[i].timestamps.length; j++) {
									var timestamp = new Date(result[i].timestamps[j]);
									if (timestamp.getTime() == scope.currentItem.oldStartdate.getTime()) {
										result[i].timestamps[j] = scope.currentItem.start.toString();
									}
									else if (timestamp.getTime() == scope.currentItem.oldEnddate.getTime()) {
										result[i].timestamps[j] = scope.currentItem.end.toString();
									}
								}
								if(timeline) {
						 		 	timeline.destroy();
								}
								init(result);
								$('#updatetimelineModal').modal('hide');
								break;
							}
						}
					}
					else
						data.result = 'error';
				});
				
			}
			scope.loadTimeline(scope.currentDate);
			
			function init (data) {
			
				result = data;
				var max = new Date(result[0].timestamps[0]);
				var min = new Date(result[0].timestamps[0]);
				var arr = [];
				var groups = [];
			
				var count = 0;
				for (var i=0; i<result.length; i++) {
					groups.push({
						id: i,
						content: [result[i].empSmartId,
						 result[i].totalHours]
					});
				
					for (var j=0; j<result[i].timestamps.length; j=j+2) {
				
						var login = new Date(result[i].timestamps[j]);
						var logout = new Date(result[i].timestamps[j + 1]);
					
						if (max && logout > max) max = logout;
						if (min && login < min) min = login;
					
						arr.push({
							id: count++,
							start: login,
							end: logout,
							group: i,
							editable: true,
							empSmartId: result[i].empSmartId
						})
					}
				}
			
				var items = new vis.DataSet(arr);
			
				var options = {
					min: min,
					max: max,
					timeAxis: {scale: 'hour', step: 2},
					orientation: 'top',
					stack: false,
					editable: true,
					onUpdate: function (value, callback) {
						scope.currentItem = value;
						scope.currentItem.oldStartdate = new Date(scope.currentItem.start);
						scope.currentItem.oldEnddate = new Date(scope.currentItem.end);
						scope.$apply();
						$('#updatetimelineModal').modal('show');
					},
					onMove: function (value) {
					alert(value)
						console.log(value);
					},
					zoomable: true
				}
				//in select call, set current item and open modal
				timeline = new vis.Timeline(container, items, groups, options);
			}
		}
	} 
}])

.directive("dateTimePicker",  function ($timeout) {
	return {
		restrict: 'A',
		scope : {
			defaultDate: '=',
			date: '=ngModal'
		},
		link: function (scope, elem, attrs) {
			$(function () {
                $(elem).datetimepicker();
            });

            elem.on('dp.change', function (e) {
            	$timeout(function () {
            		scope.date = e.date.toDate();
            	})
            })

            scope.$watch('defaultDate', function (newVal) {
            	if(newVal) {
            		console.log(newVal);
            		elem.data("DateTimePicker").date(newVal);
            		//listener();
            	}
            })
		}
	}
});