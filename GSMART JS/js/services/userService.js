'use strict';

/* Services */

services.service('UserService', function ($resource, $cookies, $http) {
	this.userInfo = null;
	this.identified = false;
	$http.defaults.useXDomain = true;
	
	this.authenticate = $resource('/gsmart-rest-ws/login/auth', {}, {
		authUser : {
			method : 'POST'
		}
	});
	
	var context = this;
	
	this.getUserInfo = function (callback) {
		if (this.userInfo) {
			callback(this.userInfo);
		}
		else {
			if ($cookies.get('userId') && $cookies.get('password')) {
				this.authenticate.authUser({empSmartId : $cookies.get('userId'), empPassword: $cookies.get('password')}, function (data) {
				    //console.log(data);
					context.userInfo = data;
					callback(data);
				});
			}
			else {
				callback(null);
			}
		}
	}
	
	this.unidentifyUser = function () {
		$cookies.remove('userId');
		$cookies.remove('password');
		this.userInfo = null;
	}
});





