'use strict';

services.service('HolidayService', function ($resource) {
	this.holidays = null;
	
	this.getHolidays = function () {
		return $resource('/gsmart-rest-ws/maintenance/viewHoliday', {}, 
		{holidaylist : {method : 'POST'}})
		
	}
	
});


services.factory('AddHolidayFactory', function($resource) {
	return $resource('/gsmart-rest-ws/maintenance/addHoliday/48750005', {}, {
		holidayres : {
			method : 'POST'
		}
		
		})
});


services.factory('EditHolidayFactory', function($resource) {
	return $resource('/gsmart-rest-ws/maintenance/updateHoliday/48750005', {}, {
		holiday : {
			method : 'POST'
		}
		
		})
});