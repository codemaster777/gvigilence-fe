'use strict';

services.service('shifttimeServices', function ($resource) {
	this.shifts = null;
	this.add=null
	
	this.getShifts = function () {
	 console.log("ShiftCtroller.serv...................");
		return $resource('/gsmart-rest-ws/shift/viewshift', {}, 
		{shiftlist : {method : 'POST'}})
		
	}

	this.deleteShifts = function (data) {
	 console.log("delete ShiftCtroller.serv..................." +data);
		return $resource('/gsmart-rest-ws/shift/deleteShift/'+ data,  {}, 
		{deleteshift : {method : 'POST'}})
		}
		
	
	
	});


services.service('addshiftServices', function ($resource) {
	 this.addShifts=function(shiftTime){
	 return $resource('/gsmart-rest-ws/shift/addShift', {}, 
		{
			add : {method : 'POST', 
			data: shiftTime,
		        headers:{'Content-Type':'application/json; charset=UTF-8'} 
			}
		})
	}
	});
	
	services.service('editshifttimeServices', function ($resource) {
	 		this.editShifts=function(shiftTime){
			return $resource('/gsmart-rest-ws/shift/UpdateShift/'+shiftTime.shiftId, {}, {
			edit : {	method : 'POST'	}
			
		})
			}
	});