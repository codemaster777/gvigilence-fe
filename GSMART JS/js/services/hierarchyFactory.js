'use strict';

services.factory('HierarchyFactory', function($resource) {
	return $resource('/gsmart-rest-ws/maintenance/viewHierarchy', {}, {
		hierarchylist : {
			method : 'POST'
		}
	})
});

services.factory('addHierarchyFactory', function($resource) {
	console.log('addhierarchy called');
	return $resource('/gsmart-rest-ws/maintenance/addHierarchy/48750005', {}, {
		addhierarchy : {method : 'POST'}
	})
});

services.factory('editHierarchyFactory', function($resource) {
	console.log('edithierarchy called');
	return $resource('/gsmart-rest-ws/maintenance/updateHierarchy/48750005', {}, {
		edithierarchy : {method : 'POST'}
	})
});
