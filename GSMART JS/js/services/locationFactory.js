'use strict';

services.factory('LocationFactory', function($resource) {
	console.log('location Factory  called');
	return $resource('/gsmart-rest-ws/maintenance/viewLocation', {}, {
		loc : {
			method : 'POST'
		}
	})
});

services.factory('AddLocationFactory', function($resource) {
	return $resource('/gsmart-rest-ws/maintenance/addLocation/48750005', {}, {
		addloc : {
			method : 'POST'
		}
		
		})
});

services.factory('EditLocationFactory', function($resource) {
	return $resource('/gsmart-rest-ws/maintenance/UpdateLocation/48750005', {}, {
		editloc : {
			method : 'POST'
		}
		
		})
});

services.factory('DeleteLocationFactory', function($resource) {
	return $resource('/gsmart-rest-ws/maintenance/deleteLocation/48750005', {}, {
		deleteloc : {
			method : 'POST'
		}
		
		})
});