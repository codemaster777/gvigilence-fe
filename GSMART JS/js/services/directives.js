'use strict';

/* Directives */


angular.module('ngdemo.directives', []).
  directive('appVersion', ['version', function(version) {
    return function(scope, elm, attrs) {
      elm.text(version);
    };
  }])
  .directive ('fitAvailableHeight', function () {
		return function(scope, elm, attrs) {
		     function adjustHeight() {
			  var headerHeight = $('header').outerHeight();
			  var footerHeight = $('.footer').outerHeight();
			  var viewportHeight = $(window).height();
			  console.log(headerHeight, footerHeight, viewportHeight)
			
			  elm.css('minHeight', viewportHeight - headerHeight - footerHeight);
			}
			
		    $(document).ready(adjustHeight);
			$(window).resize(adjustHeight);
		};
  })

  .directive('timeline', function () {
	return function (scope, element, attrs) {
		    console.log('inside directive');
			//var container = element[0];
			//console.log(container);
			var today = new Date();
			var yesterday = new Date();
			yesterday.setDate(today.getDate()-1); 

			// Create a DataSet (allows two way data-binding)
			var items = new vis.DataSet([
				{id: 4, content: 'item 4', start: yesterday, end: today, className: 'timeline-item'}
			]);

			// Configuration for the Timeline
			var options = {
				min: new Date().setDate(today.getDate() - 2),
				max: today
			};

			// Create a Timeline
			var timeline = new vis.Timeline(element, items, options);
		
	}
  });