'use strict';

services.service('ProfileService', function ($resource, $cookies) {
		
	this.getProfile = function () {
		return $resource('/gsmart-rest-ws/profile/ownProfile/'+$cookies.get('userId'), {}, 
		{view : {method : 'POST'}})
		
	}
	
	this.editProfile = function () {
		return $resource('/gsmart-rest-ws/updateProfile/updateProfileView/'+$cookies.get('userId'), {}, 
		{edit : {method : 'POST'}})
		
	}
	
});
