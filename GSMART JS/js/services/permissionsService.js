'use strict';

services.service('permissionsService', function ($resource) {
	
	
	this.getpermissions = function () {
		return $resource('/gsmart-rest-ws/permissions/permission', {}, {view : {method : 'POST'}})
	}
	
	
	
	this.editpermissions = function () {
		return $resource('/gsmart-rest-ws/permissions/updatePermission', {}, {edit : {method : 'POST'}})
	}
	
	
});

services.factory('DeletePermissionFactory', function($resource) {
	return $resource('/gsmart-rest-ws/permissions/delete', {}, {
		deletepermissions : {
			method : 'POST'
		}
		
		})
});








