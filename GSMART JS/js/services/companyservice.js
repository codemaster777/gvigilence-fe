'use strict';

services.service('Companyservices', function($resource) {
    this.company = null;

    this.getcompanys = function() {
        console.log("companyController.serv...................");
        return $resource('/gsmart-rest-ws/Company/ViewCompany', {}, {
            companylist: {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json;charset=utf-8"
                },
                data: {
                    test: 'test'
                }
            }
        })
    }
});

services.factory('AddCompanyfactory', function($resource) {

    return $resource('/gsmart-rest-ws/Company/addCompany', {}, {
        addcompanys: {
            method: 'POST',
            headers: {
                "Content-Type": "application/json;charset=utf-8"
            }
        }
    })

});


services.factory('EditCompanyfactory', function($resource) {
    return $resource('/gsmart-rest-ws/Company/UpdateCompany', {}, {
        company: {
            method: 'POST'
        }

    })
});


services.factory('DeleteCompanyfactory', function($resource) {
    return $resource('/gsmart-rest-ws/Company/DeleteCompany', {}, {
        deletecompany: {
            method: 'POST'
        }

    })
});
