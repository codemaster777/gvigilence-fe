'use strict';

services.factory('LeaveFactory', function($resource) {
	console.log('location Factory  called');
	return $resource('/gsmart-rest-ws/maintenance/viewLocation', {}, {
		loc : {
			method : 'POST'
		}
	})
});

services.factory('AddLeaveFactory', function($resource) {
	return $resource('/gsmart-rest-ws/maintenance/addLocation/48750005', {}, {
		addloc : {
			method : 'POST'
		}
		
		})
});

services.factory('EditLeaveFactory', function($resource) {
	return $resource('/gsmart-rest-ws/maintenance/UpdateLocation/48750005', {}, {
		editloc : {
			method : 'POST'
		}
		
		})
});

services.factory('DeleteLeaveFactory', function($resource) {
	return $resource('/gsmart-rest-ws/maintenance/deleteLocation/48750005', {}, {
		deleteloc : {
			method : 'POST'
		}
		
		})
});