'use strict';

services.service('BandService', function ($resource) {
	this.bands = null;
	
	this.getBands = function () {
		return $resource('/gsmart-rest-ws/maintenance/bandView', {}, {view : {method : 'POST'}})
	}
	
	this.addBand = function (band) {
	
		return $resource('/gsmart-rest-ws/maintenance/addBand/48750005', {}, {
			add : {	method : 'POST'	}
			
		})
	}
	
	this.editBand = function (empId) {
		return $resource('/gsmart-rest-ws/maintenance/updateBand/' + empId, {}, {
			edit : {method : 'POST'	}
			
		})
	}
	
	
});










