'use strict';

services.service('timesheetServices', function ($resource) {
		
	this.getTimes = function () {
		return $resource('/gsmart-rest-ws/timesheetCtrl/dayReport', {}, 
		{timesheetList : {method : 'POST'}})
		
	}

	this.update = function ()  {
		return $resource('/gsmart-rest-ws/timesheetCtrl/updateTimesheet', {},
			{timesheetData : { method : 'POST',
			headers: {
                    "Content-Type": "application/json;charset=utf-8"
                }}})
		
		}

	this.getWeekly = function () {
		return $resource('/gsmart-rest-ws/timesheetCtrl/weeklyReport', {}, 
		{weekly : {method : 'POST'}})
		
	}

	this.getMonthly = function () {
		return $resource('/gsmart-rest-ws/timesheetCtrl/monthlyReport', {}, 
		{monthly : {method : 'POST'}})
		
	}
	
});
