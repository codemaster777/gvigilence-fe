
'use strict';



app.controller('LoginCtrl', [ '$rootScope', '$scope', 'UserService', 
	'$location', '$cookies', function($rootScope, $scope, UserService, $location, $cookies) {
	
		$scope.currentPage = $location.path();
		$scope.loginError = null;
		
		$scope.authenticateUser = function() {
			 
			
			UserService.authenticate.authUser($scope.login, function (data) {
				
				if (data.result == 0) {
					$scope.$parent.userInfo = data;
					UserService.userInfo = data;
					$cookies.put('userId', $scope.login.empSmartId);
					$cookies.put('password', $scope.login.empPassword);
					$location.path('/dashboard');
				}
				else if(data.result == 1) {
					$location.path('/ChangePassword');
				}
				else if(data.result == 2) {
					$scope.loginError = data.message;
				}
				else {
					$scope.loginError = data.message;
				}
			});
		}
	} 
]);			