
app.controller('RegistrationCtrl', ['$rootScope','$scope', '$location','RegistrationService',
    function ($rootScope, $scope,  $location, RegistrationService) {
        console.log ( 'registrationCtrl ctrl called' );
		
		
			RegistrationService.getProfiles().view(function(data) {
			console.log ( 'data :', typeof data, data );
			$scope.profileInfo = data;
			RegistrationService.profiles = data;
			});
			
			
			$scope.addProfile = function (Profile) {
			$scope.$parent.profile = null;
			$location.path('/adduser');
		}

			$scope.editProfile = function (prof) {
			var dob = new Date(prof.dob);
			var joinDate = new Date(prof.joinDate);
			var passIssue = new Date(prof.passIssueDate);
			var passLast = new Date(prof.passExpDate);
			prof.dob = dob;
			prof.joinDate = joinDate;
			prof.passIssueDate = passIssue;
			prof.passExpDate = passLast;
			$scope.$parent.profile = prof;
			$location.path('/editUser');
		}
		
		
		
	}
	]);	
	
	app.controller('AddUserCtrl', ['$scope','$rootScope','$location', 'AddProfileFactory', 'RegistrationService',  
	function ($scope,$rootScope,$location, AddProfileFactory, RegistrationService) {
		
		
		$scope.save= function() {
			console.log($scope.profile)
				AddProfileFactory.add($scope.profile, function(data) {
					console.log('AddUserCtrl ctrl called:', typeof data, data);
					$scope.profiles = data;
					$location.path('/registration')
					});
			}
			/* $scope.save= function() {
			console.log('data :',  $scope.profile);
			RegistrationFactory.register($scope.profile, function(data) {
				console.log('data :', typeof data, data);
				$scope.RegistrationInfo = data;
				$location.path('/registration')
				
				
			});
		} */
		
		RegistrationService.band.get(function (band) {
			console.log(band.result);
			$scope.bands = band.result;
			$scope.bands.forEach(function(band) {
				$scope.bands[band.id.emp_Band]
			});
			
		});
				
    }]);
	
	app.controller('EditUserCtrl', ['$scope','$rootScope','$location', 'EditProfileFactory',   
	function ($scope,$rootScope,$location, EditProfileFactory) {
		
		
		$scope.save= function() {
				console.log("Edit user :", $scope.profile);
		        console.log("Edit user :", $scope.profile.dob);
				/* $scope.profile.dob = $scope.profile.dob.toString();
				$scope.profile.passIssueDate = $scope.profile.passIssueDate.toString();
				$scope.profile.passExpDate = $scope.profile.passExpDate.toString();
				$scope.profile.joinDate = $scope.profile.joinDate.toString(); */
				console.log("Edit user1 :", typeof $scope.profile.dob);
				EditProfileFactory.edit($scope.profile, function(data) {
					console.log('EditUserCtrl ctrl called:', typeof data, data);
					$scope.profiles = data;
					$location.path('/registration')
					});
			}
				
    }]);
	
	
	
	
		