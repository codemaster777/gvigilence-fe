'use strict';

app.controller('HolidayCtrl', [ '$rootScope', '$scope', 'HolidayService','$location', 
	function($rootScope, $scope, HolidayService, $location)
	{
		HolidayService.getHolidays().holidaylist(function(response)
		{
			console.log('HolidayCtrl ctrl called:', typeof response, response);
			$scope.holidayInfo = response;
			HolidayService.holidays = response;
		});
		
		$scope.addholiday = function (holiday) {
			$scope.$parent.currentholiday = holiday;
			console.log($scope.currentholiday);
			$location.path('/addHoliday');
		}
		
		$scope.editHoliday = function (holi) {
			$scope.$parent.holiday = holi;
			console.log($scope.currentholiday);
			$location.path('/editHoliday');
		}
		
		
	}]
	);
	
	app.controller('AddHolidayCtrl', [ '$rootScope', '$scope', 'AddHolidayFactory','$location', 
		function($rootScope, $scope, AddHolidayFactory, $location) {
			
			
			$scope.saveHoliday= function() {
				AddHolidayFactory.holidayres($scope.holiday, function(data) {
					console.log('AddHolidayCtrl ctrl called:', typeof data, data);
					$scope.holidayres = data;
					$location.path('/holiday')
				});
			}
		} ]);		
		
		
		app.controller('EditHolidayCtrl', [ '$rootScope', '$scope', 'EditHolidayFactory','$location', 
			function($rootScope, $scope, EditHolidayFactory, $location) {
				
				
				$scope.saveHoliday= function() {
					EditHolidayFactory.holiday($scope.holiday, function(data) {
						console.log('EditHolidayCtrl ctrl called:', typeof data, data);
						$scope.holidayres = data;
						$location.path('/holiday')
					});
				}
			} ]);																