'use strict';


app.controller('BandCtrl', ['$rootScope','$scope', 'BandService', '$location',
      function ($rootScope, $scope, BandService, $location) {
      
	  
	  BandService.getBands().view(function(data)
	   {
			console.log ( 'data :', typeof data, data );
			$scope.BandInfo = data;
			BandService.bands = data;
			});
			
			
			$scope.addband = function (Band) {
			$scope.$parent.currentband = Band;
			console.log($scope.currentband);
			$location.path('/addBand');
		}

			$scope.editband = function (ban) {
			$scope.$parent.band = ban;
			$location.path('/editBand');
		}
		
	  }]);


app.controller('AddBandCtrl', ['$scope','$rootScope','$location', 'BandService',   
	function ($scope,$rootScope,$location, BandService) {
		
		
		$scope.save= function() {
				BandService.addBand($scope.band.emp_smart_id).add($scope.band, function(data) {
					console.log('AddBandCtrl ctrl called:', typeof data, data);
					$scope.bandss = data;
					$location.path('/band')
					});
			}
				
    }]);
	
	
	app.controller('EditBandCtrl', ['$scope','$rootScope','$location', 'BandService',   
	function ($scope,$rootScope,$location, BandService) {
		
		
		$scope.save= function() {
		        console.log("Edit band :", $scope.band);
				BandService.editBand($scope.band.emp_smart_id).edit($scope.band.id, function(data) {
					console.log('EditBandCtrl ctrl called:', typeof data, data);
					$scope.bandss = data;
					$location.path('/band')
					});
			}
				
    }]);
