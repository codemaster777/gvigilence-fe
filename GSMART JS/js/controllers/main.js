'use strict';

/* Controllers */

var app = angular.module('ngdemo.controllers', []);

// Clear browser cache (in development mode)

// http://stackoverflow.com/questions/14718826/angularjs-disable-partial-caching-on-dev-machine
app.run(function($rootScope, $templateCache) {
	$rootScope.$on('$viewContentLoaded', function() {
		$templateCache.removeAll();
	});
});

app.controller('mainCtrl', ['$scope', '$location', 'UserService', '$rootScope', function ($scope, $location, UserService, $rootScope) {
		$scope.location = null;
		$scope.holiday=null;
		$scope.band = null;
		$scope.hierarchy = null;
		$scope.permissions = null;
		$scope.profile = null;
		
		
		$scope.showPrevNext = $location.path() !== '/login';
		$scope.showUserInfo = $location.path() !== '/login';
		
		$scope.userInfo = null;
		$scope.loginError = null;
		
		$rootScope.$on('$routeChangeSuccess', function () {
			$scope.showPrevNext = $location.path() !== '/login';
			$scope.showUserInfo = $location.path() !== '/login';
		});
		
		$rootScope.$on('$routeChangeStart', function (route) {
			UserService.getUserInfo(function (data) {
			    console.log(data);
				if (!data) $location.path('/login');
			});
		});
		
		$scope.logout = function () {
			UserService.unidentifyUser();
			$location.path('/login');
		}
		
		$scope.back = function () {
			window.history.back();
		}
		
		$scope.forward = function () {
			window.history.forward();
		}
		
		UserService.getUserInfo(function (data) {
			if (!data) $location.path('/login');
		});
		
	}]); 






