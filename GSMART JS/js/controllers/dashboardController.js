'use strict';


app.controller('DashboardCtrl', [ '$rootScope', '$scope', '$location', 'UserService',
	function($rootScope, $scope, $location, UserService) {
		
		$scope.moduleIcons = {
			Maintenance : 'fa-cogs',
			TimeSheet: 'fa-clock-o',
			OrganisationStructure: 'fa-user'
		}
		
		$scope.filterModule = function (module) {
		
			return $scope.moduleIcons[module.moduleName] ? true : false;
		}
		
	    $scope.userInfo = null;
		UserService.getUserInfo(function (data) {
			$scope.userInfo = data;
		});
	} ]);