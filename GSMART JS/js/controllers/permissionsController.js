'use strict';


app.controller('permissionsCtrl', ['$rootScope','$scope', 'permissionsService', '$location','DeletePermissionFactory',
	function ($rootScope, $scope, permissionsService, $location, DeletePermissionFactory) {
		
		permissionsService.getpermissions().view(function(data)
		{
			console.log ( 'data :', typeof data, data );
			$scope.permissionsInfo = data;
			
		});
		
		$scope.filterModule = function (module) {
			return $scope.moduleIcons[module.moduleName] ? true : false;
		}
		
		
		$scope.anyDuplicates = function (permissions) {
			var dupes = false;
			console.log($scope.permissions);
			for (var i = 0; i<$scope.permissionsInfo.permissions.length; i++) {
			
				$scope.cnt = 0;
				
				for (var j = 0; j < $scope.permissionsInfo.permissions.length; j++) {
					
					if ($scope.permissionsInfo.permissions[i].empRole == $scope.permissionsInfo.permissions[j].empRole) {
						$scope.cnt++;
					}
					
					if ($scope.cnt > 1) dupes = false;
					console.log($scope.cnt, j);
				}
			}
			return dupes;
		}
		
		
		
		
	
	
	
	
	$scope.addpermissions = function () 
	{
		console.log('permissionsCtrl')
		$location.path('/addpermissions');
	}
	
	$scope.editpermissions = function (permission) 
	{
		$scope.$parent.permissions = permission;
		$location.path('/editpermissions');
	}
	
	$scope.deletepermission = function (permissions) {	
		DeletePermissionFactory.deletepermissions(permissions, function(data) {
			for (var i=0; i<$scope.permissionsInfo.permissions.length; i++) {
				if ($scope.permissionsInfo.permissions[i].in_Z == permissions.in_Z) {
					$scope.permissionsInfo.permissions.splice(i, 1);
				}
				}
		});
		
	}
	
	}]);
	
	
	
	app.controller('editpermissionsCtrl', ['$rootScope','$scope', 'permissionsService', '$location',
		function ($rootScope, $scope, permissionsService, $location) {
			
			$scope.save= function() {
		        
				permissionsService.editpermissions().edit($scope.permissions, function(data) {
					console.log(':', typeof data, data);
					$scope.permis = data;
					$location.path('/permissions')
				});
			}
			
			
			
		}]);
		
		
		app.controller('viewPermissionsCtrl', ['$rootScope','$scope', 'permissionsService', '$location',
			function ($rootScope, $scope, permissionsService, $location) {
			}]);
			
			
						