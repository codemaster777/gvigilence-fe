app.controller('HierarchyCtrl', [ '$rootScope', '$scope', 'HierarchyFactory', '$location', 
	function($rootScope, $scope, HierarchyFactory, $location) 
	{
		
		HierarchyFactory.hierarchylist(function(data) {
			console.log('HierarchyCtrl ctrl called:', typeof data, data);
			$rootScope.hierarchyInfo = data;
			
		});
		
		$scope.addhierarchy = function (hierarchy) {
			$scope.$parent.hierarchy = hierarchy;
			console.log('hierarchy called');
			$location.path('/addHierarchy');
		}
		
		$scope.editHierarchy = function (hier) {
			$scope.$parent.hierarchy = hier;
			console.log($scope.$parent.hierarchy);
			console.log('hierarchy called');
			$location.path('/editHierarchy');
		}
	}]);
	
		
	app.controller('AddHierarchyCtrl', [ '$rootScope', '$scope', 'addHierarchyFactory',	'$location', 
		function($rootScope, $scope, addHierarchyFactory, $location) {
			
			$scope.add = function() {
				addHierarchyFactory.addhierarchy($scope.hierarchy, function(data) {
					$location.path('/hierarchy');
				})
			}
			
		}]);	
		
		
		app.controller('EditHierarchyCtrl', [ '$rootScope', '$scope', 'editHierarchyFactory',	'$location', 
			function($rootScope, $scope, editHierarchyFactory, $location) {
				
				$scope.edit = function() {					
				editHierarchyFactory.edithierarchy($scope.hierarchy.id, function(data) {
						$location.path('/hierarchy');
					})
				}
				
			}]);					