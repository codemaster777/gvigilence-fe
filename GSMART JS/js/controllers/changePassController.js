
'use strict';



app.controller('changePassController', [ '$rootScope', '$scope', 'ChangePassService', 
	'$location', '$cookies', function($rootScope, $scope, ChangePassService, $location, $cookies) {
	
		$scope.currentPage = $location.path();
		$scope.passError = null;
		$scope.passSuccess = null;
		
		$scope.changePass = function() {
			 
			
			ChangePassService.changePassword.change($scope.login, function (data) {

				 if (data.result == 'success') {	
					$scope.passSuccess = 'password changed';
					$location.path('/login');
				}
				else {
					$scope.passError = data.result;
					
				}
			});
		}
	} ]);			