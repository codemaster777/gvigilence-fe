'use strict';

app.controller('timesheetCtrl', [ '$rootScope', '$scope', 'timesheetServices', '$location',
	function($rootScope, $scope, timesheetServices, $location)
	{
		timesheetServices.getTimes().timesheetList(function(response)
		{
			console.log('TimeSheetCtrl ctrl called:', typeof response, response);
			$scope.timeData = response;
			timesheetServices.times = response
		});
		
	

		
		
		// DOM element where the Timeline will be attached
	}]
	);

app.controller('timesheetWeeklyCtrl', [ '$rootScope', '$scope', 'timesheetServices', '$location',
	function($rootScope, $scope, timesheetServices, $location)
	{	
		$rootScope.currentDate = null;

		$scope.getEmpDayRecord = function (empTimeRecord) {
			$rootScope.currentDate = new Date(empTimeRecord.date);
			console.log("weekly date ", $rootScope.currentDate)
			$location.path('/timesheet');
		}
		
		timesheetServices.getWeekly().weekly(function(response)
		{
			console.log('TimeSheetCtrl weekly called:', typeof response, response);
			$scope.weekData = response.result;
		});

		
		
		// DOM element where the Timeline will be attached
	}]
	);

app.controller('timesheetMonthlyCtrl', [ '$rootScope', '$scope', 'timesheetServices', '$location',
	function($rootScope, $scope, timesheetServices, $location)
	{
		
		timesheetServices.getMonthly().monthly(function(response)
		{
			console.log('TimeSheetCtrl monthly called:', typeof response, response);
			$scope.monthData = response.result[0];
		});

		
		
		// DOM element where the Timeline will be attached
	}]
	);