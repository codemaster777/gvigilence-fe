'use strict';


app.controller('LocationCtrl', [ '$rootScope', '$scope', 'LocationFactory',	'$location', 'DeleteLocationFactory', 
	function($rootScope, $scope, LocationFactory, $location, DeleteLocationFactory ) {
	
	
		LocationFactory.loc(function(data) {
			console.log('data :', typeof data, data);
			$scope.locationInfo = data;
		});
		
		
		$scope.editLocation = function (loc) {
			$scope.$parent.location = loc;
			$location.path('/editLocation');
		}
		
		$scope.deleteLocation = function (location) {	
			DeleteLocationFactory.deleteloc(location, function(data) {
				for (var i=0; i<$scope.locationInfo.result.length; i++) {
					if ($scope.locationInfo.result[i].in_Z == location.in_Z) {
						$scope.locationInfo.result.splice(i, 1);
					}
				}
			});
		
		}
		
	}
	
]);

app.controller('AddLocationCtrl', [ '$rootScope', '$scope', 'AddLocationFactory','$location', 
	function($rootScope, $scope, AddLocationFactory, $location) {
		
		$scope.$parent.location = null;
		$scope.save= function() {
			AddLocationFactory.addloc($scope.location, function(data) {
				console.log('AddlocationCtrl ctrl called:', typeof data, data);
				$scope.loc = data;
				$location.path('/location')
			});
		}
	} ]);		
	
app.controller('EditLocationCtrl', [ '$rootScope', '$scope', 'EditLocationFactory','$location', 
	function($rootScope, $scope, EditLocationFactory, $location) {
			
			
			$scope.save= function() {
				EditLocationFactory.editloc($scope.location, function(data) {
					console.log('EditLocationCtrl ctrl called:', typeof data, data);
					$scope.loc = data;
					$location.path('/location')
				});
			}
		} ]);																