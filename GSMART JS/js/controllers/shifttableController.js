'use strict';

app.controller('ShiftCtrl', [ '$scope', 'shifttimeServices','$location', 
	function($scope, shifttimeServices,$location)
	{
	console.log("controller..............:");
		shifttimeServices.getShifts().shiftlist(function(response)
		{
			console.log('Shift ctrl called:', typeof response, response);
			$scope.ShiftInfo = response;
			shifttimeServices.shifts = response;
			
			
		});
		
		
		$scope.deleteShifts= function(response,index){
			
			shifttimeServices.deleteShifts(response.shiftId,index).deleteshift(function()
			{
				console.log("delete row of id..............:" +response);
				$scope.ShiftInfo.result.splice( index, 1 );
			});
		}
		
		$scope.editShift = function (shift,index) {
			console.log("shift............"+ shift.shiftName +"index:" +index);
			$scope.$parent.shifts = shift;
			console.log("currenttimeshift............" );
			$location.path('/editshift');
		}
		
		}]
	);
	
	app.controller('AddShiftCtrlr', ['$scope', 'addshiftServices','$location', 
		function($scope, addshiftServices, $location) {
			
			$scope.save= function(data){
			
				$scope.shiftTime.shiftDuration=($scope.shiftTime.endTime-$scope.shiftTime.beginTime)/3600000;
				$scope.shiftTime.break1Duration=(($scope.shiftTime.break1EndTime-$scope.shiftTime.break1BeginTime)*18/5)/36000/6;
				$scope.shiftTime.break2Duration=(($scope.shiftTime.break2EndTime-$scope.shiftTime.break2BeginTime)*18/5)/36000/6;;
				
				if($scope.shiftTime.break1==true || $scope.shiftTime.break1beginTime!="undefined" || $scope.shiftTime.break2beginTime!="undefined"  )
				{
					$scope.shiftTime.break1beginTime=$scope.shiftTime.break1BeginTime;
					$scope.shiftTime.break1EndTime=$scope.shiftTime.break1EndTime;
					}
						else{
				//alert("Break1 to be checked");
					//$location.path('/addshift');
					return 0;
					}
           if($scope.shiftTime.break2==true)
				{
					$scope.shiftTime.break2beginTime=$scope.shiftTime.break2BeginTime;
					$scope.shiftTime.break2EndTime=$scope.shiftTime.break2EndTime;
					}
				else if ($scope.shiftTime.break2==false){
					return 0;
			}
			else{
				alert("Break2 to be checked");
					$location.path('/addshift');
					}
				data=angular.toJson(data);
				console.log("json data:"+data);
				
				addshiftServices.addShifts($scope.shiftTime).add(data, function (data) {
					//$scope.shiftInfo = data;
					$location.path('/shiftview');
				});
			}
		}]);
		
		
		
		
		app.controller('EditShiftCtrl', [ '$scope', 'editshifttimeServices','$location', 
			function($scope, editshifttimeServices, $location) {
				console.log("EditShiftCtrl ctrl called before save");
				
				$scope.save= function() {
					console.log("EditShiftCtrl ctrl called after save");
					
					
					editshifttimeServices.editShifts($scope.shifts).edit($scope.shifts, function(data) {
						console.log('EditShiftCtrl ctrl called:', typeof data, data);
						$scope.shifts = data;
						$location.path('/shiftview');
						
					});
				}
			} ]);																
			
			