'use strict';

// Declare app level module which depends on filters, and services
angular.module('ngdemo', ['ngRoute', 'ngCookies', 'ngResource',  'ngdemo.filters', 'ngdemo.services', 'ngdemo.directives', 'ngdemo.controllers']).
    config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/login', {templateUrl: 'partials/login.html', controller: 'LoginCtrl'});
        $routeProvider.when('/dashboard', {templateUrl: 'partials/dashboard.html', controller: 'DashboardCtrl'});
        $routeProvider.when('/maintenance', {templateUrl: 'partials/maintenance.html'});
		
        $routeProvider.when('/location', {templateUrl: 'partials/location.html', controller: 'LocationCtrl'});
		$routeProvider.when('/addLocation', {templateUrl: 'partials/addLocation.html', controller: 'AddLocationCtrl'});
		$routeProvider.when('/editLocation', {templateUrl: 'partials/editLocation.html', controller: 'EditLocationCtrl'});
		
		
        $routeProvider.when('/band', {templateUrl: 'partials/band.html', controller: 'BandCtrl'});
		$routeProvider.when('/addBand/', {templateUrl: 'partials/addBand.html', controller: 'AddBandCtrl'});
		$routeProvider.when('/editBand', {templateUrl: 'partials/editBand.html', controller: 'EditBandCtrl'});
		
        $routeProvider.when('/holiday', {templateUrl: 'partials/holiday.html', controller: 'HolidayCtrl'});
		$routeProvider.when('/addHoliday', {templateUrl: 'partials/addHoliday.html', controller: 'AddHolidayCtrl'});
		$routeProvider.when('/editHoliday', {templateUrl: 'partials/editHoliday.html', controller: 'EditHolidayCtrl'});
		
        $routeProvider.when('/privilageInput', {templateUrl: 'partials/privilageInput.html', controller: 'PrivilageInpCtrl'});
        $routeProvider.when('/privilage', {templateUrl: 'partials/privilage.html', controller: 'PrivilageCtrl'});
						
		$routeProvider.when('/hierarchy', {templateUrl: 'partials/hierarchy.html', controller: 'HierarchyCtrl'});
		$routeProvider.when('/addHierarchy', {templateUrl: 'partials/addHierarchy.html', controller: 'AddHierarchyCtrl'});
		$routeProvider.when('/editHierarchy', {templateUrl: 'partials/editHierarchy.html', controller: 'EditHierarchyCtrl'});
		
		$routeProvider.when('/company', {templateUrl: 'partials/company.html', controller: 'CompanyCtrl'});
		$routeProvider.when('/addcompanystar', {templateUrl: 'partials/addcompanystar.html', controller: 'AddCompanyCtrl'});
		$routeProvider.when('/updatecompanystar', {templateUrl: 'partials/updatecompanystar.html', controller: 'EditCompanyCtrl'});
		
		//$routeProvider.when('/registration', {templateUrl: 'partials/registration.html', controller: 'RegistrationCtrl'});
		$routeProvider.when('/registration', {templateUrl: 'partials/registration.html', controller: 'RegistrationCtrl'});
		$routeProvider.when('/adduser', {templateUrl: 'partials/adduser.html', controller: 'AddUserCtrl'});
		$routeProvider.when('/editUser', {templateUrl: 'partials/editUser.html', controller: 'EditUserCtrl'});
		
		$routeProvider.when('/permissions', {templateUrl: 'partials/permissions.html', controller: 'permissionsCtrl'});
		
		$routeProvider.when('/timesheet', {templateUrl: 'partials/timesheet.html', controller: 'timesheetCtrl'});
		$routeProvider.when('/weekly', {templateUrl: 'partials/weekly.html', controller: 'timesheetWeeklyCtrl'});
		$routeProvider.when('/monthly', {templateUrl: 'partials/monthly.html', controller: 'timesheetMonthlyCtrl'});
		
		$routeProvider.when('/viewPermissions', {templateUrl: 'partials/viewPermissions.html', controller: 'permissionsCtrl'});
		$routeProvider.when('/addpermissions', {templateUrl: 'partials/addpermissions.html', controller: 'addpermissionsCtrl'});
		$routeProvider.when('/editpermissions', {templateUrl: 'partials/editpermissions.html', controller: 'editpermissionsCtrl'});
		
		$routeProvider.when('/shiftview', {templateUrl: 'partials/shiftview.html', controller: 'ShiftCtrl'});
		$routeProvider.when('/addshift', {templateUrl: 'partials/addshift.html', controller: 'AddShiftCtrlr'});
		$routeProvider.when('/editshift', {templateUrl: 'partials/editshift.html', controller: 'EditShiftCtrl'});
		
		$routeProvider.when('/payroll', {templateUrl: 'partials/payroll.html'});
		$routeProvider.when('/CreateSalaryStructure', {templateUrl: 'partials/CreateSalaryStructure.html', controller: 'CreateSalaryStructureCtrl'});
		//$routeProvider.when('/addSalaryStructure', {templateUrl: 'partials/addSalaryStructure.html', controller: 'addSalaryStructureCtrl'});
		$routeProvider.when('/editSalaryStructure', {templateUrl: 'partials/editSalaryStructure.html',controller:'editSalaryStructureCtrl'});
		
		$routeProvider.when('/ChangePassword', {templateUrl: 'partials/ChangePassword.html', controller: 'changePassController'});
		$routeProvider.when('/forgotpass', {templateUrl: 'partials/forgotpass.html', controller: 'forgotpassController'});
		
		$routeProvider.when('/selfProfile', {templateUrl: 'partials/selfProfile.html',controller:'ProfileCtrl'});
		
        $routeProvider.otherwise({redirectTo: '/login'});
    }]);
